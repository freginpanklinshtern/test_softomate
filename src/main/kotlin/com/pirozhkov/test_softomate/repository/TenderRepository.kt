package com.pirozhkov.test_softomate.repository

import com.pirozhkov.test_softomate.model.repository.TenderModel
import io.swagger.annotations.Api
import org.bson.types.ObjectId
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository

@Api(value = "Tender entity")
@RepositoryRestResource(path = "tender_db")
@Repository
interface TenderRepository: MongoRepository<TenderModel, ObjectId> {
    fun findByClientId(clientId: Long, pageable: Pageable): List<TenderModel>
}