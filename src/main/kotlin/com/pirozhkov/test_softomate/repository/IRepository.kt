package com.pirozhkov.test_softomate.repository

import com.pirozhkov.test_softomate.model.repository.ClientModel
import com.pirozhkov.test_softomate.model.repository.TenderModel

interface IRepository {
    /**
     * Сохранение данных
     */
    fun saveClient(client: ClientModel)
    fun saveTender(tender: TenderModel)

    /**
     * Получение данных
     */
    fun getClient(id: Long): ClientModel?
    fun getTenders(id: Long, page: Int = 0, limit: Int = 0): List<TenderModel>

    fun getClients(page: Int = 0, limit: Int = 0): List<ClientModel>
    fun getTenders(page: Int = 0, limit: Int = 0): List<TenderModel>

    /**
     * Чистка данных
     */
    fun cleanClients()
    fun cleanTenders()
    fun clean() {
        cleanClients()
        cleanTenders()
    }

    companion object {
        val Instance by lazy { InMemoryRepository.Instance }
    }
}