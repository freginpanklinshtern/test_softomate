package com.pirozhkov.test_softomate.repository

import com.pirozhkov.test_softomate.model.repository.ClientModel
import com.pirozhkov.test_softomate.model.repository.TenderModel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class MongoDbRepository(
    @Autowired private val clientRepository: ClientRepository,
    @Autowired private val tenderRepository: TenderRepository
) : IRepository {

    override fun saveClient(client: ClientModel) {
        clientRepository.save(client)
    }
    override fun saveTender(tender: TenderModel) {
        tenderRepository.save(tender)
    }

    override fun getClient(id: Long): ClientModel? =
        clientRepository.findById(id)

    override fun getTenders(id: Long, page: Int, limit: Int): List<TenderModel> =
        if (limit <= 0) tenderRepository.findByClientId(id, Pageable.unpaged())
        else tenderRepository.findByClientId(id, PageRequest.of(page, limit)).toList()

    override fun getClients(page: Int, limit: Int): List<ClientModel> =
        if (limit <= 0) clientRepository.findAll()
        else clientRepository.findAll(PageRequest.of(page, limit)).toList()

    override fun getTenders(page: Int, limit: Int): List<TenderModel> =
        if (limit <= 0) tenderRepository.findAll()
        else tenderRepository.findAll(PageRequest.of(page, limit)).toList()

    override fun cleanClients() {
        clientRepository.deleteAll()
    }

    override fun cleanTenders() {
        tenderRepository.deleteAll()
    }

}