package com.pirozhkov.test_softomate.repository

import com.pirozhkov.test_softomate.model.repository.ClientModel
import io.swagger.annotations.Api
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository

@Api(value = "Client entity")
@RepositoryRestResource(path = "client_db")
@Repository
interface ClientRepository: MongoRepository<ClientModel, ObjectId> {
    fun findById(id: Long): ClientModel?
}