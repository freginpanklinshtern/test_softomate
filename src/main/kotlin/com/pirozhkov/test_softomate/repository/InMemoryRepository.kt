package com.pirozhkov.test_softomate.repository

import com.pirozhkov.test_softomate.model.repository.ClientModel
import com.pirozhkov.test_softomate.model.repository.TenderModel

/**
 * Used, when Mongo repository didn't exist
 */
class InMemoryRepository : IRepository {
    private val clients: MutableSet<ClientModel> = mutableSetOf()
    private val tenders: MutableSet<TenderModel> = mutableSetOf()

    override fun saveClient(client: ClientModel) {
        clients.add(client)
    }

    override fun saveTender(tender: TenderModel) {
        tenders.add(tender)
    }

    override fun getClient(id: Long): ClientModel? =
        clients.firstOrNull { it.id == id }

    private fun <T> pagination(list: Collection<T>, page: Int, limit: Int) =
        list
            .let {
                when {
                    page > 0 && limit > 0 -> it.drop(page * limit)
                    page > 0 && limit <= 0 -> it.drop(page * DEFAULT_LIMIT)
                    else -> it
                }
            }
            .let {
                when {
                    limit > 0 -> it.take(limit)
                    else -> it
                }
            }

    override fun getTenders(id: Long, page: Int, limit: Int): List<TenderModel> =
        pagination(tenders.filter { it.clientId == id }, page, limit).toList()

    override fun getClients(page: Int, limit: Int): List<ClientModel> =
        pagination(clients, page, limit).toList()

    override fun getTenders(page: Int, limit: Int): List<TenderModel> =
        pagination(tenders, page, limit).toList()

    override fun cleanClients() =
        clients.clear()

    override fun cleanTenders() =
        tenders.clear()

    companion object {
        private const val DEFAULT_LIMIT: Int = 10

        val Instance by lazy { InMemoryRepository() }
    }
}