package com.pirozhkov.test_softomate.controller

import com.pirozhkov.test_softomate.getFromUrl
import com.pirozhkov.test_softomate.parser.JsonParser
import com.pirozhkov.test_softomate.readLocal
import com.pirozhkov.test_softomate.repository.IRepository
import com.pirozhkov.test_softomate.repository.MongoDbRepository
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.net.URL

@Api(value = "Upload data to database from json source", description = "Uploading data from json files")
@RestController
@RequestMapping("/json")
class JsonSaverController(@Autowired private val repository: IRepository) {

    @RequestMapping("/save_clients", method = [RequestMethod.POST, RequestMethod.GET])
    fun saveClients(
        @RequestParam(value = "url", defaultValue = DEFAULT_CLIENTS_JSON_URL) url: String,
        @RequestParam(value = "use_local", defaultValue = "false") useLocal: Boolean
    ) = JsonParser.Instance.parseClients(if (useLocal) LOCAL_CLIENTS else getFromUrl(url))
        .forEach { repository.saveClient(it) }

    @RequestMapping("/save_tenders", method = [RequestMethod.POST, RequestMethod.GET])
    fun saveTenders(
        @RequestParam(value = "url", defaultValue = DEFAULT_TENDERS_JSON_URL) url: String,
        @RequestParam(value = "use_local", defaultValue = "false") useLocal: Boolean
    ) = JsonParser.Instance.parseTenders(if (useLocal) LOCAL_TENDERS else getFromUrl(url))
        .forEach { repository.saveTender(it) }

    companion object {
        private val LOCAL_CLIENTS by lazy { readLocal("clients.json") }

        private val LOCAL_TENDERS by lazy { readLocal("tenders.json") }

        private const val DEFAULT_CLIENTS_JSON_URL =
            "https://s3.eu-central-1.amazonaws.com/jobs.softomate.com/testdata/ad-merchant.json"

        private const val DEFAULT_TENDERS_JSON_URL =
            "https://s3.eu-central-1.amazonaws.com/jobs.softomate.com/testdata/ad-tenders.json"
    }
}