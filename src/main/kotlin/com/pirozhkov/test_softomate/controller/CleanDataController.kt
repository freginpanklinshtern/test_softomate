package com.pirozhkov.test_softomate.controller

import com.pirozhkov.test_softomate.repository.IRepository
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.net.URL

@Api(value = "Clean data from database", description = "API for truncate collections")
@RestController
@RequestMapping("/clean")
class CleanDataController(@Autowired private val repository: IRepository) {

    @RequestMapping("/clients", method = [RequestMethod.DELETE, RequestMethod.GET])
    fun cleanClients() = repository.cleanClients()

    @RequestMapping("/tenders", method = [RequestMethod.DELETE, RequestMethod.GET])
    fun cleanTenders() = repository.cleanTenders()

    @RequestMapping("/all", method = [RequestMethod.DELETE, RequestMethod.GET])
    fun clean() = repository.clean()
}