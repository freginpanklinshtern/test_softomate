package com.pirozhkov.test_softomate.controller

import com.pirozhkov.test_softomate.repository.IRepository
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@Api(value = "Get raw data from database", description = "API for getting raw data from database")
@RestController
@RequestMapping("/get_raw")
class GetRawDataController(@Autowired private val repository: IRepository) {

    @RequestMapping("/clients", method = [RequestMethod.POST, RequestMethod.GET])
    fun getClients(
        @RequestParam(value = "page", defaultValue = "0") page: Int,
        @RequestParam(value = "limit", defaultValue = "0") limit: Int
    ) = repository.getClients(page = page, limit = limit)

    @RequestMapping("/tenders", method = [RequestMethod.POST, RequestMethod.GET])
    fun getTenders(
        @RequestParam(value = "id") id: Long? = null,
        @RequestParam(value = "page", defaultValue = "0") page: Int,
        @RequestParam(value = "limit", defaultValue = "0") limit: Int
    ) =
        if (id != null) repository.getTenders(id = id, page = page, limit = limit)
        else repository.getTenders(page = page, limit = limit)

    @RequestMapping("/client", method = [RequestMethod.POST, RequestMethod.GET])
    fun getClient(
        @RequestParam(value = "id", required = true) id: Long
    ) = repository.getClient(id = id)

}