package com.pirozhkov.test_softomate.controller

import com.pirozhkov.test_softomate.model.aggregated.AggregatedClient
import com.pirozhkov.test_softomate.model.aggregated.AggregatedResponse
import com.pirozhkov.test_softomate.model.aggregated.AggregatedTender
import com.pirozhkov.test_softomate.repository.IRepository
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@Api(value = "Get data from database", description = "API for getting data from database")
@RestController
@RequestMapping("/get")
class GetDataController(@Autowired private val repository: IRepository) {

    @RequestMapping("/clients", method = [RequestMethod.POST, RequestMethod.GET])
    fun getClients(
        @RequestParam(value = "page", defaultValue = "0") page: Int,
        @RequestParam(value = "limit", defaultValue = "0") limit: Int
    ) = repository.getClients(page = page, limit = limit)
        .map { model ->
            AggregatedClient.build(
                model,
                model.id?.let { id -> repository.getTenders(id).map { AggregatedTender.build(it) } }
            )
        }
        .let { clients -> AggregatedResponse(clients) }

    @RequestMapping("/tenders", method = [RequestMethod.POST, RequestMethod.GET])
    fun getTenders(
        @RequestParam(value = "page", defaultValue = "0") page: Int,
        @RequestParam(value = "limit", defaultValue = "0") limit: Int
    ) = AggregatedResponse(tenders = repository.getTenders(page = page, limit = limit).map { AggregatedTender.build(it) })

}