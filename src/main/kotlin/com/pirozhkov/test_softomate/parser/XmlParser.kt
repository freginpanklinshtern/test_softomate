package com.pirozhkov.test_softomate.parser

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.pirozhkov.test_softomate.model.repository.ClientModel
import com.pirozhkov.test_softomate.model.repository.TenderModel
import com.pirozhkov.test_softomate.model.xml.common.XmlCjApiModel

class XmlParser(mapper: XmlMapper = XmlMapper()): JacksonParser(mapper) {
    companion object {
        val Instance by lazy { XmlParser() }
    }

    override fun parseClients(source: String): List<ClientModel> =
        mapper.readValue<XmlCjApiModel>(source).advertisers?.list
            ?.map { ClientModel.build(it) }
            ?: emptyList()

    override fun parseTenders(source: String): List<TenderModel> =
        mapper.readValue<XmlCjApiModel>(source).links?.list
            ?.map { TenderModel.build(it) }
            ?: emptyList()
}