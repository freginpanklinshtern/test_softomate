package com.pirozhkov.test_softomate.parser

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.KotlinModule

abstract class JacksonParser(val mapper: ObjectMapper): IParser {
    init {
        mapper.enable(SerializationFeature.INDENT_OUTPUT)
        mapper.registerModule(KotlinModule())
    }
}