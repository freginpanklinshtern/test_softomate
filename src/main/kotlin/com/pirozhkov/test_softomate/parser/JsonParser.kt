package com.pirozhkov.test_softomate.parser

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.pirozhkov.test_softomate.model.repository.ClientModel
import com.pirozhkov.test_softomate.model.repository.TenderModel
import com.pirozhkov.test_softomate.model.json.merchant.JsonMerchantsModel
import com.pirozhkov.test_softomate.model.json.offer.JsonOffersModel

class JsonParser(mapper: ObjectMapper = ObjectMapper()): JacksonParser(mapper) {
    companion object {
        val Instance by lazy { JsonParser() }
    }

    override fun parseClients(source: String): List<ClientModel> =
        mapper.readValue<JsonMerchantsModel>(source).results
            ?.map { ClientModel.build(it) }
            ?: emptyList()

    override fun parseTenders(source: String): List<TenderModel> =
        mapper.readValue<JsonOffersModel>(source).results
            ?.map { TenderModel.build(it) }
            ?: emptyList()
}