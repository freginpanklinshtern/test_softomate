package com.pirozhkov.test_softomate.parser

import com.pirozhkov.test_softomate.model.repository.ClientModel
import com.pirozhkov.test_softomate.model.repository.TenderModel

interface IParser {
    fun parseClients(source: String): List<ClientModel>
    fun parseTenders(source: String): List<TenderModel>
}