package com.pirozhkov.test_softomate

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories


@SpringBootApplication
@EnableMongoRepositories("com.pirozhkov.test_softomate.repository")
class Service

fun main(args: Array<String>) {
    runApplication<Service>(*args)
}