package com.pirozhkov.test_softomate.model.aggregated

import java.time.Instant

data class AggregatedResponse(
    val clients: List<AggregatedClient>? = null,
    val tenders: List<AggregatedTender>? = null,
    val timestamp: Instant = Instant.now()
) {
    val clientsCount = clients?.count()
    val tendersCount = clients?.sumBy { it.tenders?.count() ?: 0 } ?: tenders?.count()
}