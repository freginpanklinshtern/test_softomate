package com.pirozhkov.test_softomate.model.aggregated

import com.pirozhkov.test_softomate.model.repository.TenderModel

/**
 * Aggregated model for end-client
 * Removed technical fields
 */
data class AggregatedTender(
    val description: String? = null,
    val categories: List<String>? = null,
    val url: String? = null,
    val startDate: String? = null,
    val endDate: String? = null,
    val promoCode: String? = null,
    val name: String? = null
) {
    companion object {
        fun build(model: TenderModel) =
            AggregatedTender(
                description = model.description,
                categories = model.categories,
                url = model.clickUrl,
                startDate = model.startDate,
                endDate = model.endDate,
                promoCode = model.promoCode,
                name = model.name
            )
    }
}