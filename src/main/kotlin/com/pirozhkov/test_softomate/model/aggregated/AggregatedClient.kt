package com.pirozhkov.test_softomate.model.aggregated

import com.pirozhkov.test_softomate.model.repository.ClientModel

/**
 * Aggregated model for end-client
 * Removed technical fields
 */
data class AggregatedClient(
    val name: String? = null,
    val url: String? = null,
    val description: String? = null,
    val tenders: List<AggregatedTender>? = null
) {
    companion object {
        fun build(model: ClientModel, tenders: List<AggregatedTender>? = null) =
            AggregatedClient(
                name = model.name,
                url = model.url,
                description = model.description,
                tenders = tenders
            )
    }
}