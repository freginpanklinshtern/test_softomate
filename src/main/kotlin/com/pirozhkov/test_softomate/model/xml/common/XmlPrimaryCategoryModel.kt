package com.pirozhkov.test_softomate.model.xml.common

data class XmlPrimaryCategoryModel(
    val parent: String? = null,
    val child: String? = null
)