package com.pirozhkov.test_softomate.model.xml.link

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.pirozhkov.test_softomate.model.xml.common.XmlMetaModel

data class XmlLinksModel(
    @JacksonXmlProperty(localName = "link")
    @JacksonXmlElementWrapper(localName = "link", useWrapping = false)
    val list: List<XmlLinkModel>? = null
): XmlMetaModel()