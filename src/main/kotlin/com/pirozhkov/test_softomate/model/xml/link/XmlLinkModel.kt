package com.pirozhkov.test_softomate.model.xml.link

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

data class XmlLinkModel(
    @JacksonXmlProperty(localName = "advertiser-id")
    var advertiserId: Long? = null,
    @JacksonXmlProperty(localName = "advertiser-name")
    val advertiserName: String? = null,
    @JacksonXmlProperty(localName = "category")
    val category: String? = null,
    @JacksonXmlProperty(localName = "language")
    val language: String? = null,
    @JacksonXmlProperty(localName = "description")
    val description: String? = null,
    @JacksonXmlProperty(localName = "destination")
    val destination: String? = null,
    @JacksonXmlProperty(localName = "promotion-end-date")
    val promotionEndDate: String? = null,
    @JacksonXmlProperty(localName = "promotion-start-date")
    val promotionStartDate: String? = null,
    @JacksonXmlProperty(localName = "promotion-type")
    val promotionType: String? = null,
    @JacksonXmlProperty(localName = "coupon-code")
    val couponCode: String? = null,
    @JacksonXmlProperty(localName = "relationship-status")
    val relationshipStatus: String? = null,
    @JacksonXmlProperty(localName = "sale-commission")
    val saleCommission: String? = null,
    @JacksonXmlProperty(localName = "seven-day-epc")
    val sevenDayEpc: String? = null,
    @JacksonXmlProperty(localName = "three-month-epc")
    val threeMonthEpc: String? = null,
    @JacksonXmlProperty(localName = "clickUrl")
    val clickUrl: String? = null
)