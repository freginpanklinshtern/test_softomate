package com.pirozhkov.test_softomate.model.xml.advertiser

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.pirozhkov.test_softomate.model.xml.common.XmlPrimaryCategoryModel

data class XmlAdvertiserModel(
    @JacksonXmlProperty(localName = "advertiser-id")
    var advertiserId: Long? = null,
    @JacksonXmlProperty(localName = "account-status")
    val accountStatus: String? = null,
    @JacksonXmlProperty(localName = "seven-day-epc")
    val sevenDayEpc: Double? = null,
    @JacksonXmlProperty(localName = "three-month-epc")
    val threeMonthEpc: Double? = null,
    @JacksonXmlProperty(localName = "language")
    val language: String? = null,
    @JacksonXmlProperty(localName = "advertiser-name")
    val advertiserName: String? = null,
    @JacksonXmlProperty(localName = "program-url")
    val programUrl: String? = null,
    @JacksonXmlProperty(localName = "relationship-status")
    val relationshipStatus: String? = null,
    @JacksonXmlProperty(localName = "network-rank")
    val networkRank: Long? = null,
    @JacksonXmlProperty(localName = "primary-category")
    val primaryCategory: XmlPrimaryCategoryModel? = null
)