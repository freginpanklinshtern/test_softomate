package com.pirozhkov.test_softomate.model.xml.common

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement
import com.pirozhkov.test_softomate.model.xml.advertiser.XmlAdvertisersModel
import com.pirozhkov.test_softomate.model.xml.link.XmlLinksModel

@JacksonXmlRootElement(localName = "cj-api")
data class XmlCjApiModel(
    @JacksonXmlProperty(localName = "advertisers")
    val advertisers: XmlAdvertisersModel? = null,

    @JacksonXmlProperty(localName = "links")
    val links: XmlLinksModel? = null
)