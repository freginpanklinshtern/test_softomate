package com.pirozhkov.test_softomate.model.xml.advertiser

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.pirozhkov.test_softomate.model.xml.common.XmlMetaModel

data class XmlAdvertisersModel(
    @JacksonXmlProperty(localName = "advertiser")
    @JacksonXmlElementWrapper(localName = "advertiser", useWrapping = false)
    val list: List<XmlAdvertiserModel>? = null
): XmlMetaModel()