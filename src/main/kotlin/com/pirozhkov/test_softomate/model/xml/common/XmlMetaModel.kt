package com.pirozhkov.test_softomate.model.xml.common

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

abstract class XmlMetaModel {
    @JacksonXmlProperty(isAttribute = true, localName = "total-matched")
    val totalMatched: String? = null
    @JacksonXmlProperty(isAttribute = true, localName = "records-returned")
    val recordsReturned: String? = null
    @JacksonXmlProperty(isAttribute = true, localName = "page-number")
    val pageNumber: String? = null
}