package com.pirozhkov.test_softomate.model.json.offer

import com.fasterxml.jackson.annotation.JsonProperty
import com.pirozhkov.test_softomate.model.json.common.JsonCampaignModel
import com.pirozhkov.test_softomate.model.json.common.JsonCategoryModel
import com.pirozhkov.test_softomate.model.json.common.JsonDiscountTypeModel
import com.pirozhkov.test_softomate.model.json.common.JsonRegionModel

data class JsonOfferModel(
    @JsonProperty("id")
    val id: Long? = null,
    @JsonProperty("status")
    val status: String? = null,
    @JsonProperty("rating")
    val rating: String? = null,
    @JsonProperty("date_start")
    val dateStart: String? = null,
    @JsonProperty("campaign")
    val campaign: JsonCampaignModel? = null,
    @JsonProperty("short_name")
    val shortName: String? = null,
    @JsonProperty("exclusive")
    val exclusive: Boolean? = null,
    @JsonProperty("name")
    val name: String? = null,
    @JsonProperty("date_end")
    val dateEnd: String? = null,
    @JsonProperty("promocode")
    val promoCode: String? = null,
    @JsonProperty("regions")
    val regions: List<String>? = null,
    @JsonProperty("discount")
    val discount: String? = null,
    @JsonProperty("types")
    val types: List<JsonDiscountTypeModel>? = null,
    @JsonProperty("image")
    val image: String? = null,
    @JsonProperty("frameset_link")
    val framesetLink: String? = null,
    @JsonProperty("goto_link")
    val gotoLink: String? = null,
    @JsonProperty("species")
    val species: String? = null,
    @JsonProperty("categories")
    val categories: List<JsonCategoryModel>? = null,
    @JsonProperty("description")
    val description: String? = null
)