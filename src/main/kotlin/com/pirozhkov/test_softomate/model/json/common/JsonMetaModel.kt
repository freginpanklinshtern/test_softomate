package com.pirozhkov.test_softomate.model.json.common

import com.fasterxml.jackson.annotation.JsonProperty

data class JsonMetaModel(
    @JsonProperty("count")
    val count: Long? = null,
    @JsonProperty("limit")
    val limit: Long? = null,
    @JsonProperty("offset")
    val offset: Long? = null
)