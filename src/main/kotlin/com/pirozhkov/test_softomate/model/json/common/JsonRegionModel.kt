package com.pirozhkov.test_softomate.model.json.common

import com.fasterxml.jackson.annotation.JsonProperty

data class JsonRegionModel(
    @JsonProperty("region")
    val region: String? = null
)