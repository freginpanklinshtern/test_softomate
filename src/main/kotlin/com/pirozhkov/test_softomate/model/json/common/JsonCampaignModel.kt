package com.pirozhkov.test_softomate.model.json.common

import com.fasterxml.jackson.annotation.JsonProperty

data class JsonCampaignModel(
    @JsonProperty("id")
    val id: Long? = null,
    @JsonProperty("name")
    val name: String? = null,
    @JsonProperty("site_url")
    val siteUrl: String? = null
)