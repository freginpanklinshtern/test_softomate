package com.pirozhkov.test_softomate.model.json.offer

import com.fasterxml.jackson.annotation.JsonProperty
import com.pirozhkov.test_softomate.model.json.common.JsonMetaModel

data class JsonOffersModel(
    @JsonProperty("results")
    val results: List<JsonOfferModel>? = null,
    @JsonProperty("_meta")
    val meta: JsonMetaModel? = null
)