package com.pirozhkov.test_softomate.model.json.merchant

import com.fasterxml.jackson.annotation.JsonProperty
import com.pirozhkov.test_softomate.model.json.common.JsonMetaModel

data class JsonMerchantsModel(
    @JsonProperty("results")
    val results: List<JsonMerchantModel>? = null,
    @JsonProperty("_meta")
    val meta: JsonMetaModel? = null
)