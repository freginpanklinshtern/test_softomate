package com.pirozhkov.test_softomate.model.json.merchant

import com.fasterxml.jackson.annotation.JsonProperty
import com.pirozhkov.test_softomate.model.json.common.JsonCategoryModel
import com.pirozhkov.test_softomate.model.json.common.JsonRegionModel
import java.sql.RowIdLifetime

data class JsonMerchantModel(
    @JsonProperty("id")
    val id: Long? = null,
    @JsonProperty("goto_cookie_lifetime")
    val gotoCookieLifetime: Long? = null,
    @JsonProperty("rating")
    val rating: String? = null,
    @JsonProperty("image")
    val image: String? = null,
    @JsonProperty("currency")
    val currency: String? = null,
    @JsonProperty("activation_date")
    val activationDate: String? = null,
    @JsonProperty("avg_hold_time")
    val avgHoldTime: Long? = null,
    @JsonProperty("connection_status")
    val connectionStatus: String? = null,
    @JsonProperty("gotolink")
    val gotoLink: String? = null,
    @JsonProperty("site_url")
    val siteUrl: String? = null,
    @JsonProperty("regions")
    val regions: List<JsonRegionModel>? = null,
    @JsonProperty("status")
    val status: String? = null,
    @JsonProperty("description")
    val description: String? = null,
    @JsonProperty("modified_date")
    val modifiedDate: String? = null,
    @JsonProperty("categories")
    val categories: List<JsonCategoryModel>? = null,
    @JsonProperty("name")
    val name: String? = null
)