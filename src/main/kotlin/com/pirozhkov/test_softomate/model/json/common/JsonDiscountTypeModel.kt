package com.pirozhkov.test_softomate.model.json.common

import com.fasterxml.jackson.annotation.JsonProperty

data class JsonDiscountTypeModel(
    @JsonProperty("id")
    val id: String? = null,
    @JsonProperty("name")
    val name: String? = null
)