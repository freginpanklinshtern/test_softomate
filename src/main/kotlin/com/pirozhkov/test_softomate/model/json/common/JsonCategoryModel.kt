package com.pirozhkov.test_softomate.model.json.common

import com.fasterxml.jackson.annotation.JsonProperty

data class JsonCategoryModel(
    @JsonProperty("language")
    val language: String? = null,
    @JsonProperty("id")
    val id: Long? = null,
    @JsonProperty("parent")
    val parent: JsonCategoryModel? = null,
    @JsonProperty("name")
    val name: String? = null
)