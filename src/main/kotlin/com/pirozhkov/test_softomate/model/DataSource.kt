package com.pirozhkov.test_softomate.model

enum class DataSource {
    NONE,
    JSON,
    XML;
}