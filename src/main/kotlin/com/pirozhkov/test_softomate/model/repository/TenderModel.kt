package com.pirozhkov.test_softomate.model.repository

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.pirozhkov.test_softomate.model.DataSource
import com.pirozhkov.test_softomate.model.json.offer.JsonOfferModel
import com.pirozhkov.test_softomate.model.xml.link.XmlLinkModel
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document(collection = "tender")
data class TenderModel(
    @Id
    val objId: ObjectId? = null,

    /**
     * Common fields
     */
    @JsonProperty("client_id")
    @Field
    val clientId: Long? = null,
    @JsonProperty("client_name")
    @Field
    val clientName: String? = null,
    @JsonProperty("categories")
    @Field
    @JacksonXmlElementWrapper(useWrapping = false)
    val categories: List<String>? = null,
    @JsonProperty("languages")
    @Field
    val languages: List<String>? = null,
    @JsonProperty("description")
    @Field
    val description: String? = null,
    @JsonProperty("client_url")
    @Field
    val clientUrl: String? = null,
    @JsonProperty("start_date")
    @Field
    val startDate: String? = null,
    @JsonProperty("end_date")
    @Field
    val endDate: String? = null,
    @JsonProperty("types")
    @Field
    @JacksonXmlElementWrapper(useWrapping = false)
    val types: List<String>? = null,
    @JsonProperty("promo_code")
    @Field
    val promoCode: String? = null,
    @JsonProperty("status")
    @Field
    val status: String? = null,
    @JsonProperty("discount")
    @Field
    val discount: String? = null,
    @JsonProperty("click_url")
    @Field
    val clickUrl: String? = null,
    @JsonProperty("source")
    @Field
    val source: DataSource? = null,

    /**
     * Fields from JSON Source
     */
    @JsonProperty("id")
    @Field
    val id: Long? = null,
    @JsonProperty("rating")
    @Field
    val rating: String? = null,
    @JsonProperty("short_name")
    @Field
    val shortName: String? = null,
    @JsonProperty("exclusive")
    @Field
    val exclusive: Boolean? = null,
    @JsonProperty("name")
    @Field
    val name: String? = null,
    @JsonProperty("image")
    @Field
    val image: String? = null,
    @JsonProperty("frameset_link")
    @Field
    val frameSetLink: String? = null,
    @JsonProperty("species")
    @Field
    val species: String? = null,

    /**
     * Fields from XML Source
     */
    @JsonProperty("seven_day_epc")
    @Field
    val sevenDayEpc: String? = null,
    @JsonProperty("three_month_epc")
    @Field
    val threeMonthEpc: String? = null
) {
    companion object {
        fun build(model: JsonOfferModel) =
            TenderModel(
                clientId = model.campaign?.id,
                clientName = model.campaign?.name,
                categories = model.categories?.mapNotNull { it.name },
                languages = model.regions?.map { it.toLowerCase() },
                description = model.description,
                clientUrl = model.campaign?.siteUrl,
                startDate = model.dateStart,
                endDate = model.dateEnd,
                types = model.types?.mapNotNull { it.name },
                promoCode = model.promoCode,
                status = model.status,
                discount = model.discount,
                clickUrl = model.gotoLink,
                source = DataSource.JSON,
                id = model.id,
                rating = model.rating,
                shortName = model.shortName,
                exclusive = model.exclusive,
                name = model.name,
                image = model.image,
                frameSetLink = model.framesetLink,
                species = model.species
            )

        fun build(model: XmlLinkModel) =
            TenderModel(
                clientId = model.advertiserId,
                clientName = model.advertiserName,
                categories = listOfNotNull(model.category),
                languages = listOfNotNull(model.language),
                description = model.description,
                clientUrl = model.destination,
                startDate = model.promotionStartDate,
                endDate = model.promotionEndDate,
                types = listOfNotNull(model.promotionType),
                promoCode = model.couponCode,
                status = model.relationshipStatus,
                discount = model.saleCommission,
                clickUrl = model.clickUrl,
                source = DataSource.XML,
                sevenDayEpc = model.sevenDayEpc,
                threeMonthEpc = model.threeMonthEpc
            )
    }
}