package com.pirozhkov.test_softomate.model.repository

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.pirozhkov.test_softomate.model.DataSource
import com.pirozhkov.test_softomate.model.json.merchant.JsonMerchantModel
import com.pirozhkov.test_softomate.model.xml.advertiser.XmlAdvertiserModel
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document(collection = "client")
data class ClientModel(
    @Id
    val objId: ObjectId? = null,

    /**
     * Common fields
     */
    @JsonProperty("id")
    @Field
    val id: Long? = null,
    @JsonProperty("name")
    @Field
    val name: String? = null,
    @JsonProperty("status")
    @Field
    val status: String? = null,
    @JsonProperty("connection_status")
    @Field
    val connectionStatus: String? = null,
    @JsonProperty("languages")
    @Field
    @JacksonXmlElementWrapper(useWrapping = false)
    val languages: List<String>? = null,
    @JsonProperty("categories")
    @Field
    @JacksonXmlElementWrapper(useWrapping = false)
    val categories: List<String>? = null,
    @JsonProperty("url")
    @Field
    val url: String? = null,
    @JsonProperty("source")
    @Field
    val source: DataSource? = null,

    /**
     * Fields from JSON Source
     */
    @JsonProperty("description")
    @Field
    val description: String? = null,
    @JsonProperty("image")
    @Field
    val image: String? = null,
    @JsonProperty("goto_cookie_lifetime")
    @Field
    val gotoCookieLifetime: Long? = null,
    @JsonProperty("rating")
    @Field
    val rating: String? = null,
    @JsonProperty("currency")
    @Field
    val currency: String? = null,
    @JsonProperty("activation_date")
    @Field
    val activationDate: String? = null,
    @JsonProperty("avg_hold_time")
    @Field
    val avgHoldTime: Long? = null,
    @JsonProperty("goto_link")
    @Field
    val gotoLink: String? = null,
    @JsonProperty("modified_date")
    @Field
    val modifiedDate: String? = null,

    /**
     * Fields from XML Source
     */
    @JsonProperty("seven_day_epc")
    @Field
    val sevenDayEpc: Double? = null,
    @JsonProperty("three_month_epc")
    @Field
    val threeMonthEpc: Double? = null,
    @JsonProperty("network_rank")
    @Field
    val networkRank: Long? = null
) {
    companion object {
        fun build(model: JsonMerchantModel) =
            ClientModel(
                id = model.id,
                name = model.name,
                status = model.status,
                connectionStatus = model.connectionStatus,
                languages = model.regions?.mapNotNull { it.region?.toLowerCase() },
                categories = model.categories?.mapNotNull { it.name },
                url = model.siteUrl,
                source = DataSource.JSON,
                description = model.description,
                image = model.image,
                gotoCookieLifetime = model.gotoCookieLifetime,
                rating = model.rating,
                currency = model.currency,
                activationDate = model.activationDate,
                avgHoldTime = model.avgHoldTime,
                gotoLink = model.gotoLink,
                modifiedDate = model.modifiedDate
            )

        fun build(model: XmlAdvertiserModel) =
            ClientModel(
                id = model.advertiserId,
                name = model.advertiserName,
                status = model.accountStatus,
                connectionStatus = model.relationshipStatus,
                languages = listOfNotNull(model.language),
                categories = model.primaryCategory?.let { listOfNotNull(it.parent, it.child) },
                url = model.programUrl,
                source = DataSource.XML,
                sevenDayEpc = model.sevenDayEpc,
                threeMonthEpc = model.threeMonthEpc,
                networkRank = model.networkRank
            )
    }
}