package com.pirozhkov.test_softomate

import java.net.URL

fun readLocal(file: String) = ClassLoader.getSystemResource(file).readText()

fun getFromUrl(url: String) = URL(url).readText()