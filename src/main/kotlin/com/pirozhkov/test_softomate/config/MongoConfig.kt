package com.pirozhkov.test_softomate.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.data.mongodb.MongoDatabaseFactory
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory

@Configuration
class MongoConfig(@Autowired private val env: Environment) {
    @Bean
    fun mongoDbFactory(): MongoDatabaseFactory =
        SimpleMongoClientDatabaseFactory(env.getProperty("spring.data.mongodb.uri") ?: DEFAULT_CONNECTION)

    @Bean
    fun mongoTemplate(): MongoTemplate =
        MongoTemplate(mongoDbFactory())

    companion object {
        private const val DEFAULT_CONNECTION = "mongodb://localhost:27017/test_db"
    }
}