package com.pirozhkov.test_softomate

import com.pirozhkov.test_softomate.parser.JsonParser
import com.pirozhkov.test_softomate.parser.XmlParser
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.data.headers
import io.kotest.data.row
import io.kotest.data.table

internal class ParsersTest : StringSpec() {
    init {
        table(
            headers("file"),
            row("clients.json"),
            row("clients.xml"),
            row("tenders.json"),
            row("tenders.xml")
        ).forAll { fileName ->
            "parse dont throw any errors for file - $fileName" {
                shouldNotThrowAny {
                    readLocal(fileName).let { source ->
                        (if (fileName.endsWith("json")) JsonParser.Instance else XmlParser.Instance)
                            .let { parser ->
                                if (fileName.startsWith("clients")) parser.parseClients(source)
                                else parser.parseTenders(source)
                            }
                    }
                }
            }
        }
    }
}