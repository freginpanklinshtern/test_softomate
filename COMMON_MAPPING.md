## common model client mapping

|Name|Json|XML|Description|
|---|---|---|---|
|id|id|advertiser-id|Client identificator|
|name|name|advertiser-name|Client name|
|status|status|account-status|Client status|
|connectionStatus|connection_status|relationship-status|Client usage status|
|languages|regions->region|language|Client languages|
|categories|categories->name|primary-category|Client categories|
|url|site_url|program-url|Client URL|

## common model tender mapping

|Name|Json|XML|Description|
|---|---|---|---|
|client_id|campaign->id|advertiser-id|Client identificator|
|client_name|campaign->name|advertiser-name|Client name|
|categories|categories->name|category|Tender categories|
|languages|regions->region|language|Tender languages|
|description|description|description|Tender description|
|client_url|campaign->site_url|destination|Client URL|
|start_date|date_start|promotion-start-date|Tender start date|
|end_date|date_end|promotion-end-date|Tender end date|
|types|types->name|promotion-type|Tender type|
|promo_code|promocode|coupon-code|Tender PromoCode|
|status|status|relationship-status|Tender status|
|discount|discount|sale-commission|Tender discount|
|click_url|goto_link|clickUrl|Tender click URL|