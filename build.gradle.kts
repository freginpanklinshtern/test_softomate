plugins {
    kotlin("jvm") version "1.4.0"

    /**
     * Got this from https://spring.io/guides/tutorials/spring-boot-kotlin/
     */
    kotlin("plugin.jpa") version "1.4.0"
    kotlin("plugin.spring") version "1.4.0"
    id("org.springframework.boot") version "2.3.3.RELEASE"
    id("io.spring.dependency-management") version "1.0.8.RELEASE"
}

group = "com.pirozhkov.test_softomate"
version = "1.0-SNAPSHOT"

val kotlintestVersion = "4.2.3"

repositories {
    mavenCentral()
}

dependencies {
    /**
     * Got this from https://spring.io/guides/tutorials/spring-boot-kotlin/
     */
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-mustache")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    runtimeOnly("com.h2database:h2")
    runtimeOnly("org.springframework.boot:spring-boot-devtools")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    /**
     * Parse XML Jackson
     */
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml:2.11.2")

    /**
     * MongoDB
     */
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
    implementation("org.springframework.data:spring-data-rest-core")

    /**
     * Swagger
     */
    implementation("io.springfox:springfox-swagger2:3.0.0")
    implementation("io.springfox:springfox-swagger-ui:3.0.0")
    implementation("io.springfox:springfox-boot-starter:3.0.0")
    implementation("org.springframework.data:spring-data-rest-webmvc")
    implementation("io.springfox:springfox-spring-web:3.0.0")

    /**
     * Kotlintest. Got this from https://kotest.io/quick_start/
     */
    testImplementation("io.kotest:kotest-runner-junit5:$kotlintestVersion")
    testImplementation("io.kotest:kotest-assertions-core:$kotlintestVersion")
    testImplementation("io.kotest:kotest-property:$kotlintestVersion")
}

springBoot {
    mainClassName = "$group.ServiceKt"
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}